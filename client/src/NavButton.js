import React from 'react';

const NavButton = (props) => {
    if(props.disabled) 
        return <span className={`${props.type.toLowerCase()} disabled`} title={props.type}>{props.type}</span>

    return <a href="#" 
        className={props.type.toLowerCase()} 
        title={props.type}
        onClick={props.action}>{props.type}</a>
}

export default NavButton;