import React, { Component } from 'react';
import Buttons from './Buttons';
import NavButton from './NavButton';

let thumbnails = [];
const thumbnailsDir = './images/thumbnails/';

class Thumbnails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentGroup: 1,
            prevDisabled: true,
            nextDisabled: false
        };
        this.thumbGroups = Math.ceil(this.props.images.length/this.props.maxThumbs);
        this.moveNext = this.moveNext.bind(this);
        this.movePrev = this.movePrev.bind(this);
    }

    setGroup(currentGroup) {
        this.setState({
            currentGroup: currentGroup,
            nextDisabled: currentGroup + 1 > this.thumbGroups,
            prevDisabled: currentGroup === 1
        })
    }

    moveNext() {
        const newCurrentGroup = this.state.currentGroup + 1;

        this.setGroup(newCurrentGroup);
    }

    movePrev() {
        const newCurrentGroup = this.state.currentGroup - 1;
        
        this.setGroup(newCurrentGroup);
    }

    getThumbnails() {
        const startIdx = (this.state.currentGroup - 1) * this.props.maxThumbs;
        const endIdx = startIdx + this.props.maxThumbs;

        thumbnails = this.props.images.map((image, idx) => {
            if(idx >= startIdx && idx < endIdx) {
                return (
                    <a href="#" className={this.props.currentImageIdx == idx ? 'active' : ''} data-id={idx} key={image.id} onClick={this.props.selectImg} title={image.id}>
                        <img data-id={idx} src={`${thumbnailsDir}${image.thumbnail}`} width="145" height="121"/>
                        <span data-id={idx} >{image.id}</span>
                    </a>
                );
            }
        })
    }

    render() {
        this.getThumbnails();

        return (
            <div className="thumbnails">
			    <div className="group">
                    {thumbnails}
                    <NavButton type="Previous" disabled={this.state.prevDisabled} action={this.movePrev}  />
                    <NavButton type="Next" disabled={this.state.nextDisabled} action={this.moveNext} />
                </div>
            </div>
        )
    }
}

export default Thumbnails;
