import React, { Component } from 'react';
import Buttons from './Buttons';
import Thumbnails from './Thumbnails';
import Details from './Details';

const imageDir = './images/large/';

class Main extends Component {
    constructor() {
        super();
        this.state = {
            images: [],
            currentImageIdx: 0
        }

        this.maxThumbs = 4;
        this.selectImg = this.selectImg.bind(this);
    }
    componentDidMount() {
        fetch('/imageList')
            .then(res => res.json())
            .then(json => {
                this.setState({
                    images: json.images
                })
            });
    }

    selectImg(event) {
        this.setState({
            currentImageIdx: event.target.getAttribute('data-id'),
        })
    }

    render() {
        if(!Array.isArray(this.state.images) || this.state.images.length === 0) {
            return (
                <div>loading...</div>
            )
        }

        return (
            <div id="container">
                <div id="main" role="main">
                    <div id="large">
                        <div className="group">
                            <img src={`${imageDir}${this.state.images[this.state.currentImageIdx].image}`}  
                                alt="Large Image" width="430" height="360" />
                            <Details image={this.state.images[this.state.currentImageIdx]} />
                        </div>
                    </div>
                    <Thumbnails 
                        currentImageIdx={this.state.currentImageIdx}
                        maxThumbs={this.maxThumbs}
                        images={this.state.images}
                        selectImg={this.selectImg}
                        />
                </div>
            </div>
        );
    }
}

export default Main;
