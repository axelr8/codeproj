import React, { Component } from 'react';

class Buttons extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
                <img src=".\images\previous.png" onClick={this.props.setPrevImg} />
                <img src=".\images\next.png" onClick={this.props.setNextImg} />
            </div>
        )
    }
}

export default Buttons;