### Code Project
Requires nodejs v 8.x.x

Requires yarn 1.3.2

### Install node modules

```
yarn install
```

### Build the application

```
yarn build
```

### Run the application

```
yarn start
```

Navigate to localhost:5000 to view application in browser 

