const express = require('express');
const data = require('./public/data/templates.json');

const app = express();
const port = process.env.PORT || 5000;

const imagesObj = {
    images: data
}

app.use(express.static('public'));
app.get('/imageList', (req, res) => {
    res.json(imagesObj);
});

app.listen(port, () => console.log(`Listening on port ${port}`));
