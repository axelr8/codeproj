const path = require('path');


const config = {
    entry: './client/src/app.js',
    mode: 'development',
    output: {
        path: path.resolve(__dirname + '/public/', 'dist'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/, 
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015', 'react']
                }
            }
        ]
    }
};

module.exports = config;
